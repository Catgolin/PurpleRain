package purpleRain;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.Color;

import java.util.ArrayList;

public class Vue extends JPanel {
	public static void main(String[] args) {
		JFrame f = new JFrame("Purple rain");
		f.setSize(800, 800);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		new Vue(f);
	}
	private ArrayList<Goutte> gouttes;
	private ArrayList<Goutte> eclaboussures;
	private long debutIteration;
	private double temps = 0.1;
	private Vent vent;
	private boolean paintureEnCours;
	private long dureeFrame = 16;

	public Vue(JFrame fenetre) {
		fenetre.setContentPane(this);
		fenetre.setVisible(true);
		this.gouttes = new ArrayList<Goutte>();
		this.eclaboussures = new ArrayList<Goutte>();
		new Musique("/purple-rain.wav");
		this.continuer();
	}
	public void continuer() {
		while(true) {
			this.debutIteration = System.currentTimeMillis();
			for(int i = 0; i < this.gouttes.size(); i++) {
				Goutte g = this.gouttes.get(i);
				g.tomber(this.vent, this.temps);
				if(g.y() >= 100) {
					g.tomber(this.vent, -this.temps);
					for(int k = 0; k < 3; k++) {
						this.eclaboussures.add(new Goutte(g.x(), g.y()));
					}
					this.gouttes.remove(i);
				}
			}
			for(int i = 0; i < this.eclaboussures.size(); i++) {
				this.eclaboussures.get(i).tomber(this.vent, this.temps);
				if(this.eclaboussures.get(i).y() >= 100) {
					this.eclaboussures.remove(i);
				}
			}
			this.gouttes.add(new Goutte(Math.random() * 100));
			if(!this.paintureEnCours) {
				this.repaint();
			}
			try {
				long duree = System.currentTimeMillis() - this.debutIteration;
				Thread.sleep(this.dureeFrame - duree);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void paintComponent(Graphics g) {
		this.paintureEnCours = true;
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.PINK);
		for(int i = 0; i < this.gouttes.size(); i++) {
			this.afficherGoutte(g, this.gouttes.get(i));
		}
		for(int i = 0; i < this.eclaboussures.size(); i++) {
			this.afficherGoutte(g, this.eclaboussures.get(i));
		}
		this.paintureEnCours = false;
	}
	private void afficherGoutte(Graphics g, Goutte goutte) {
		int x = (int)((goutte.x() / 100.0) * (double)(this.getWidth()));
		int y = (int)((goutte.y() / 100.0) * (double)(this.getHeight()));
		g.fillOval(x, y, goutte.largeur(), goutte.hauteur());
	}
}

class Goutte {
	private static double g = 0.5;
	private double x;
	private double y;
	private double vX;
	private double vY;
	private int largeur;
	private int hauteur;
	public Goutte(double x) {
		this.x = x;
		this.y = -50;
		this.largeur = 5;
		this.hauteur = 15;
	}
	public Goutte(double x, double y) {
		this.x = x;
		this.y = y;
		this.vX = 0.5 - Math.random();
		this.vY = - (Math.random());
		this.largeur = 5;
		this.hauteur = 5;
	}
	public void tomber(Vent v, double temps) {
		this.x += this.vX * temps;
		this.y += this.vY * temps;
		this.vY += Goutte.g * temps;
	}
	public double x() {
		return this.x;
	}
	public double y() {
		return this.y;
	}
	public int largeur() {
		return this.largeur;
	}
	public int hauteur() {
		return this.hauteur;
	}
}
class Vent {}
