package purpleRain;

import java.io.InputStream;
import java.io.BufferedInputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class Musique {
	public Musique(String lien) {
		try {
			InputStream source = this.getClass().getResourceAsStream(lien);
			InputStream tampon = new BufferedInputStream(source);
			AudioInputStream entree = AudioSystem.getAudioInputStream(tampon);
			DataLine.Info info = new DataLine.Info(Clip.class, entree.getFormat());
			Clip clip = (Clip)AudioSystem.getLine(info);
			clip.open(entree);
			clip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
